#!/usr/bin/env python3

import syslog
import struct
import smbus
import sys
import time
import signal
import sys
import os
from subprocess import call
from datetime import datetime, date

min_voltage = 3.55
shutdown_delay = 180
rrt_limit = 360

def signal_handler_Ctrl_C(signal, frame):
    syslog.syslog("exiting by Ctrl-C")
    sys.exit(0)

def signal_handler_sighup(signal, frame):
    syslog.syslog("exiting by SIGHUP")
    sys.exit(0)

def signal_handler_sigterm(signal, frame):
    syslog.syslog("exiting by SIGTERM")
    sys.exit(0)

def readVoltage(bus):
     address = 0x62
     read = bus.read_word_data(address, 2)
     swapped = struct.unpack("<H", struct.pack(">H", read))[0]
     voltage = swapped * 0.305 / 1000
     return voltage

def readCapacity(bus):
     address = 0x62
     read = bus.read_word_data(address, 4)
     swapped = struct.unpack("<H", struct.pack(">H", read))[0]
     capacity = swapped * 1.0 / 256
     return capacity

def readRRT(bus):
     address = 0x62
     read = bus.read_word_data(address, 6)
     swapped = struct.unpack("<H", struct.pack(">H", read))[0]
     rrt = swapped & 0x7fff
     return rrt

def clearSleepMode(bus):
    address = 0x62
    bus.write_byte_data(address, 0x0A, 0)
    return

signal.signal(signal.SIGINT, signal_handler_Ctrl_C)
signal.signal(signal.SIGHUP, signal_handler_sighup)
signal.signal(signal.SIGTERM, signal_handler_sigterm)

bus = smbus.SMBus(1) # 0 = /dev/i2c-0 (port I2C0), 1 = /dev/i2c-1 (port I2C1)

clearSleepMode(bus)

while True:
   bat_voltage = readVoltage(bus)
   bat_capacity = readCapacity(bus)
   bat_rrt = readRRT(bus)
   message = 'Voltage ={:5.2f}V | Battery = {:5.2f}% | RRT = {:5d}Min'.format(bat_voltage,bat_capacity,bat_rrt)
   syslog.syslog(message)

   if ( bat_voltage <= min_voltage ) :
      message = 'Voltage is {:5.2f}V - too low, shutdown Raspberry Pi'.format(bat_voltage)
      syslog.syslog(message)
      call("shutdown -P +1", shell=True)
#      sys.exit(0)
      time.sleep(120)

   if ( bat_rrt >= rrt_limit) :
      syslog.syslog('External power is ON')
   else :
      message = 'External power is OFF, we wait {:3d} second before shutdown'.format(shutdown_delay)
      syslog.syslog(message)
      os.system('echo "The system will shut down in a few minutes because external power is OFF" | wall')
      time.sleep(shutdown_delay)
      bat_rrt = readRRT(bus)
      if ( bat_rrt >= rrt_limit) :
         syslog.syslog('External power returns, shutdown is canceled')
      else :
         syslog.syslog('External power is OFF, shutdown Raspberry Pi')
         call("shutdown -P +1", shell=True)
#         sys.exit(0)
         time.sleep(120)

   time.sleep(60)

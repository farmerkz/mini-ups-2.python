# Управление Power Pack Pro V1.1 UPS HAT Expansion Board Module For Raspberry Pi

Обеспечивает мониторинг наличия внешнего питания.
В случае отключения, осуществляет корректный останов системы.

Константы:
* shutdown_delay - задержка остановки ОС при отключении внешнего питания
* rrt_limit      - Значение RRT, при превышении которого считаем, что имеется вншнее питание
* min_voltage    - Минимальное напряжение батареи, при котором прозводится безусловный останов системы с задержкой в одну минуту


Установка сервиса:

* Скопировать исполняемый файл mini-ups-2 в /usr/local/sbin
* Скопировать mini-ups-2.service в /etc/systemd/system
* Выполнить команды:

```
systemctl daemon-reload
systemctl enable mini-ups-2
systemctl start mini-ups-2
```
